package aula6Threads_BanheiroSujo;

public class Program {

    public static void main(String[] args) {

        Banheiro banheiro = new Banheiro();
        Thread convidado1 = new Thread(new TarefaNumero1(banheiro), "Convidado1");
        Thread convidado2 = new Thread(new TarefaNumero2(banheiro), "Convidado2");
        Thread limpeza = new Thread(new TarefaLimpeza(banheiro), "Limpeza");
        limpeza.setDaemon(true);

        convidado1.start();
        convidado2.start();
        limpeza.start();

    }
}
