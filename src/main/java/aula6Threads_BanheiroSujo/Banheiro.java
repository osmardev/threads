package aula6Threads_BanheiroSujo;

public class Banheiro {

    Boolean ehSujo = true;

    public void fazerNumero1(){
        String nome = Thread.currentThread().getName();

        System.out.println( nome + " Batendo na porta ");
        synchronized (this){
            while (ehSujo){
                EsperaLaFora(nome);
            }
            System.out.println(nome + " Entrando no banheiro " );
            System.out.println(nome + " Fazendo coisa rapida " );
            dormirThread(5000);
            System.out.println("Dando descarga " );
            System.out.println("Lavando Mão" );
            System.out.println(nome + " Saindo do banheiro");
            ehSujo = true;
        }

    }

    public void fazerNumero2(){
        String nome = Thread.currentThread().getName();

        System.out.println( nome + " Batendo na porta ");

        synchronized (this){
            while (ehSujo){
                EsperaLaFora(nome);
            }
            System.out.println(nome + " Entrando no banheiro " );
            System.out.println(nome + " Fazendo coisa demorada ");
            dormirThread(10000);
            System.out.println("Dando descarga");
            System.out.println("Lavando Mão");
            System.out.println(nome + " Saindo do banheiro");
            ehSujo = true;
        }
    }

    private void EsperaLaFora(String nome) {
        System.out.println(nome + " Eca banheiro esta sujo");
        try {
            this.wait();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void limpeza(){
        String nome = Thread.currentThread().getName();

        System.out.println( nome + " Batendo na porta ");

        synchronized (this){
            System.out.println(nome + " Entrando no banheiro " );
            if (!ehSujo){
                System.out.println(" Banheiro esta limpo");
                System.out.println(nome + " Saindo do banheiro");
                return;
            }
            System.out.println(nome + " Limpando o banheiro ");
            dormirThread(13000);
            ehSujo = false;
            System.out.println(nome + " Saindo do banheiro");
            this.notifyAll();
        }
    }

    private void dormirThread(int i) {
        try {
            Thread.sleep(i);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


}
