package aula6Threads_BanheiroSujo;

public class TarefaNumero1 implements Runnable {

    private Banheiro banheiro;

    public TarefaNumero1(Banheiro banheiro) {
        this.banheiro = banheiro;
    }
    @Override
    public void run() {
        banheiro.fazerNumero1();
    }
}
