package aula5Threads_ListConflitoSemThreadSafe;

import java.util.List;

public class TarefaAdicionarElementoList implements Runnable{

    private List<String> lista;

    private Integer numeroThread;

    public TarefaAdicionarElementoList(List<String> lista, Integer numeroThread) {
        this.lista = lista;
        this.numeroThread = numeroThread;
    }

    @Override
    public void run() {

        for (int i = 0; i < 100; i++){
            lista.add("Convidado: " + numeroThread +" Adicionou elemento na posicao: " + i);
        }
    }
}
