package aula5Threads_ListConflitoSemThreadSafe;

import java.util.ArrayList;
import java.util.List;


public class Programa {

    public static void main(String[] args) {

        List<String> lista = new ArrayList<>();

        Integer contador = 0;

        for (int i = 0; i < 10; i++){
            Thread thread = new Thread(new TarefaAdicionarElementoList(lista, i));
            thread.start();
        }

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        for (int i = 0; i < lista.size(); i++){
            System.out.println("numero do item na lista: " + contador + " " + lista.get(i));
            contador++;
        }
    }
}
