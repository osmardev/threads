package aula2Thread_buscaTextual;

public class Program {

    public static void main(String[] args) {

        String nomeBuscado = "da";

        Thread assinanuras1 = new Thread(new TarefaBuscaTextual("assinaturas1.txt", nomeBuscado));
        Thread assinanuras2 = new Thread(new TarefaBuscaTextual("assinaturas2.txt", nomeBuscado));
        Thread autores = new Thread(new TarefaBuscaTextual("autores.txt", nomeBuscado));

        assinanuras1.start();
        assinanuras2.start();
        autores.start();
    }
}
