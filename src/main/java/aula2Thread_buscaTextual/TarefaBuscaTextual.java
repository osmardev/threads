package aula2Thread_buscaTextual;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class TarefaBuscaTextual implements Runnable {

    private String nomeArquivo;
    private String nomeBuscado;


    public TarefaBuscaTextual(String nomeArquivo, String nomeBuscado) {
        this.nomeArquivo = nomeArquivo;
        this.nomeBuscado = nomeBuscado;
    }

    @Override
    public void run() {

        File file = new File(nomeArquivo);
        try {
            Scanner scanner = new Scanner(file);
            int numeroLinha = 1;
            while (scanner.hasNextLine()){
                String linha = scanner.nextLine();

                if (linha.toLowerCase().contains(nomeBuscado.toLowerCase())){
                    System.out.println("nome do arquivo:  " + nomeArquivo + " - numero da linha: " + numeroLinha + " - nome buscado: " + linha);
                }
                numeroLinha++;
            }

            scanner.close();
        } catch (FileNotFoundException e) {
           throw new RuntimeException(e.getMessage());
        }


    }
}
