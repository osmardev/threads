package aula3Thread_conflitoDeThreads;

public class Program {

    public static void main(String[] args) {

        Banheiro banheiro = new Banheiro();
        Thread convidado1 = new Thread(new TarefaNumero1(banheiro), "Osmar");
        Thread convidado2 = new Thread(new TarefaNumero2(banheiro), "Henrique");
        Thread convidado3 = new Thread(new TarefaNumero2(banheiro), "Telriane");
        Thread convidado4 = new Thread(new TarefaNumero2(banheiro), "Melissa");

        convidado1.start();
        convidado2.start();
        convidado3.start();
        convidado4.start();
    }
}
