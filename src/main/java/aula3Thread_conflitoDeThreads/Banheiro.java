package aula3Thread_conflitoDeThreads;

public class Banheiro {



    public void fazerNumero1(){
        String nome = Thread.currentThread().getName();

        System.out.println( nome + " Batento na porta ");
        synchronized (this){
            System.out.println(nome + " Entrando no banheiro " );
            System.out.println(nome + " Fazendo coisa rapida " );
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Dando descarga " );
            System.out.println("Lavando Mão" );
            System.out.println("Saindo do banheiro");
        }
    }

    public void fazerNumero2(){
        String nome = Thread.currentThread().getName();

        System.out.println( nome + " Batento na porta ");

        synchronized (this){
            System.out.println(nome + " Entrando no banheiro " );
            System.out.println(nome + " Fazendo coisa demorada ");
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Dando descarga");
            System.out.println("Lavando Mão");
            System.out.println("Saindo do banheiro");
        }
    }
}
