package aula4Threads_ListaConflito;

public class Lista {

    String[] elementos = new String[1000];

    private int indice = 0;

    public synchronized void adicionaElementoLista(String elemento){
        elementos[indice] = elemento;
        indice++;

        try {
            Thread.sleep(5);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
            if (listaEstaCheia()){
                System.out.println("Finalizado adicão de todos elementos");
                try {
                    Thread.sleep(5000);
                    System.out.println("Lista esta cheia notificando");
                    Thread.sleep(5000);
                    this.notify();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
    }

    public int tamanho(){
        return elementos.length;
    }

    public String pegaElemento(int posicao){
        return this.elementos[posicao];
    }

    public Boolean listaEstaCheia(){
        return this.indice == tamanho();
    }

}
