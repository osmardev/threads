package aula4Threads_ListaConflito;

public class TarefaImprimirElemento implements Runnable{

    private Lista lista;

    public TarefaImprimirElemento(Lista lista) {
        this.lista = lista;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        synchronized (lista){
            try {
                System.out.println("iniciando execução tarefa de impressão de elementos");
                if (!lista.listaEstaCheia()){
                    System.out.println("Aguardando notificação para execução tarefa de impressão de elementos");
                    lista.wait();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Imprimindo elementos");
            for (int i = 0; i < lista.tamanho(); i++){
                System.out.println("posicao " + i + " " + lista.pegaElemento(i));
            }
        }
    }
}
